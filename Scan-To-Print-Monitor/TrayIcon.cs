﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.ServiceProcess;
using System.Windows.Forms;
using Scan_To_Print_Monitor.Properties;

namespace Scan_To_Print_Monitor {
    class TrayIcon : IDisposable {
        private const string STR_SERVICE = "Scan-To-Print";
        private const string STR_LOG_FILE = @"C:\eAgile\Scan-To-Print\log.txt";
        private const string STR_ERROR_LOG = @"C:\eAgile\Scan-To-Print\Scan_ErrorLog.txt";

        private NotifyIcon icoTray;
        private EventLog logApplication;
        private ServiceController conService;
        private ToolStripMenuItem mnuPreviousLog;
        private ToolStripLabel lblStatus;
        private Timer tmrStatus;
        private bool blnNotified;

        internal TrayIcon() {
            icoTray = new NotifyIcon(); // Create NotifyIcon
            conService = new ServiceController(STR_SERVICE); // Create ServiceController
            

            // Create EventLog
            logApplication = new EventLog("Application");
            logApplication.EntryWritten += logApplication_EntryWritten;
            logApplication.EnableRaisingEvents = true;

            // Create Timer
            tmrStatus = new Timer();
            tmrStatus.Interval = 1000;
            tmrStatus.Tick += tmrStatus_Tick;
            tmrStatus.Enabled = true;
        }

        internal void Display() {
            // Setup NotifyIcon
            icoTray.Icon = Resources.eAgile_icon_disabled;
            icoTray.Visible = true;
            icoTray.ContextMenuStrip = CreateContextMenu(); // Create context menu
            icoTray.BalloonTipClicked += OpenLogFile;
            SetStatus(); // Configure controls according to service statuc
        }

        private void logApplication_EntryWritten(object sender, EntryWrittenEventArgs e) {
            EventLog logLatest;
            int intIndex;
            string strOrder, strRows;

            try {
                if (conService.Status == ServiceControllerStatus.Running) // If service is running
                {
                    logApplication.EnableRaisingEvents = false; // Stop watcher
                    logLatest = new EventLog("Application"); // Get log
                    intIndex = logLatest.Entries.Count - 1; // Get latest log entry

                    if (logLatest.Entries[intIndex].Source == STR_SERVICE) // If latest entry was issued via service
                    {
                        if (logLatest.Entries[intIndex].Message.StartsWith("Tag Data")) // If the event was a barcode scan
                        {
                            strOrder = logLatest.Entries[intIndex].Message; // Get sales order
                            strRows = "Timestamp: " + DateTime.Now; // Get Timestamp

                            mnuPreviousLog.Text = strOrder + "\r\n" + strRows; // Update context menu
                            icoTray.ShowBalloonTip(5000, strOrder, strRows, ToolTipIcon.Info); // Display balloon
                        }
                        if (logLatest.Entries[intIndex].Message.StartsWith("GiS")) {
                            strOrder = logLatest.Entries[intIndex].Message; // Get GIS Disconnect Error
                            strRows = "Timestamp: " + DateTime.Now; // Get Timestamp

                            mnuPreviousLog.Text = strOrder + "\r\n" + strRows; // Update context menu
                            icoTray.ShowBalloonTip(5000, strOrder, strRows, ToolTipIcon.Info); // Display balloon
                        } 
                        if (logLatest.Entries[intIndex].Message.StartsWith("AccessIO")) {
                            strOrder = logLatest.Entries[intIndex].Message; // Get AccessIO Disconnect Error
                            strRows = "Timestamp: " + DateTime.Now; // Get Timestamp

                            mnuPreviousLog.Text = strOrder + "\r\n" + strRows; // Update context menu
                            icoTray.ShowBalloonTip(5000, strOrder, strRows, ToolTipIcon.Info); // Display balloon
                        }
                        if (logLatest.Entries[intIndex].Message.StartsWith("COM")) {
                            strOrder = logLatest.Entries[intIndex].Message; // Get AccessIO Disconnect Error
                            strRows = "Timestamp: " + DateTime.Now; // Get Timestamp

                            mnuPreviousLog.Text = strOrder + "\r\n" + strRows; // Update context menu
                            icoTray.ShowBalloonTip(5000, strOrder, strRows, ToolTipIcon.Info); // Display balloon
                        }                        
                    }

                    logApplication.EnableRaisingEvents = true; // Start watcher
                }
            } catch (Exception ex) {
                using (StreamWriter sw = new StreamWriter(STR_ERROR_LOG, true)) {
                    sw.WriteLine("########## logApplication_EntryWritten ##########");
                    sw.WriteLine(Application.ProductName);
                    sw.WriteLine(DateTime.Now);
                    sw.WriteLine(ex);
                }
                logApplication.EnableRaisingEvents = true; // Start watcher
                MessageBox.Show(ex.Message + "\r\nSee error log for more details.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tmrStatus_Tick(object sender, EventArgs e) {
            conService = new ServiceController(STR_SERVICE); // Update ServiceController
            SetStatus(); // Configure controls according to service statuc
        }

        private void SetStatus() {
            try {
                // Switch on service status
                switch (conService.Status) {
                    case ServiceControllerStatus.Running:
                    // Update controls
                    icoTray.Icon = Resources.eAgile_icon;
                    icoTray.Text = STR_SERVICE + " (" + conService.Status + ")";
                    lblStatus.Text = STR_SERVICE + " (" + conService.Status + ")";
                    lblStatus.ForeColor = Color.Green;

                    blnNotified = false; // Set flag
                    break;
                    case ServiceControllerStatus.Paused:
                    case ServiceControllerStatus.Stopped:
                    // Update controls
                    icoTray.Icon = Resources.eAgile_icon_disabled;
                    icoTray.Text = STR_SERVICE + " (" + conService.Status + ")";
                    lblStatus.Text = STR_SERVICE + " (" + conService.Status + ")";
                    lblStatus.ForeColor = Color.Red;

                    if (blnNotified == false) // If message box was not already displayed
                    {
                        blnNotified = true; // Set flag
                        MessageBox.Show(STR_SERVICE + " is not currently running.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    break;
                }
            } catch (Exception ex) {
                using (StreamWriter sw = new StreamWriter(STR_ERROR_LOG, true)) {
                    sw.WriteLine("########## SetStatus ##########");
                    sw.WriteLine(Application.ProductName);
                    sw.WriteLine(DateTime.Now);
                    sw.WriteLine(ex);
                }
                tmrStatus.Enabled = false; // Disable timer
                MessageBox.Show(ex.Message + "\r\nSee error log for more details.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Dispose();
            }
        }

        private ContextMenuStrip CreateContextMenu() {
            ContextMenuStrip menu = new ContextMenuStrip();
            ToolStripMenuItem item;

            // Log
            //menu.Items.Add(new ToolStripLabel("Previous Log Entry"));
            //menu.Items[0].Font = new Font(menu.Items[0].Font, FontStyle.Bold | FontStyle.Underline);
            mnuPreviousLog = new ToolStripMenuItem();
            if (File.Exists(STR_LOG_FILE)) {
                try {
                    using (StreamReader sr = new StreamReader(STR_LOG_FILE)) {
                        sr.ReadLine();
                        mnuPreviousLog.Text = sr.ReadLine() + "\r\n" + sr.ReadLine();
                    }
                } catch {
                    mnuPreviousLog.Text = "Tag Data: N/A\r\nTimestamp: N/A";
                }
            } else {
                mnuPreviousLog.Text = "Tag Data: N/A\r\nTimestamp: N/A";
            }
            mnuPreviousLog.Font = new Font(mnuPreviousLog.Font, FontStyle.Bold);
            mnuPreviousLog.Image = Resources.log;
            mnuPreviousLog.Click += OpenLogFile;
            menu.Items.Add(mnuPreviousLog);

            menu.Items.Add(new ToolStripSeparator()); // Separator

            // Status
            lblStatus = new ToolStripLabel();
            lblStatus.Text = "Loading...";
            lblStatus.Font = new Font(lblStatus.Font, FontStyle.Bold);
            menu.Items.Add(lblStatus);

            menu.Items.Add(new ToolStripSeparator()); // Separator

            // eAgile
            menu.Items.Add(new ToolStripLabel(Application.ProductName));
            menu.Items.Add(new ToolStripLabel("Version " + Application.ProductVersion));
            menu.Items.Add(new ToolStripLabel(Application.CompanyName + " © " + DateTime.Now.Year));

            menu.Items.Add(new ToolStripSeparator()); // Separator

            // Exit
            item = new ToolStripMenuItem();
            item.Text = "Exit Monitor";
            item.Font = new Font(item.Font, FontStyle.Bold);
            item.Image = Resources.exit;
            item.Click += exitClick;
            menu.Items.Add(item);

            return menu;
        }

        private void OpenLogFile(object sender, EventArgs e) {
            if (File.Exists(STR_LOG_FILE)) {
                Process.Start("notepad.exe", STR_LOG_FILE); // Open file in Notepad
            }
        }

        private void exitClick(object sender, EventArgs e) {
            Dispose();
        }

        public void Dispose() {
            icoTray.Dispose();
            if (conService != null)
                conService.Dispose();
            if (logApplication != null)
                logApplication.Dispose();
            if (tmrStatus != null)
                tmrStatus.Dispose();
            Application.Exit();
        }
    }
}
