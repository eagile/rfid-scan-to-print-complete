﻿using System;
using System.Windows.Forms;
using System.Diagnostics;

namespace Scan_To_Print_Monitor {
    static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            int intInstance = 0;
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            foreach (Process clsProcess in Process.GetProcesses())
            {
                if (clsProcess.ProcessName == "Scan-To-Print-Monitor")
                {
                    intInstance++;
                }
            }
            if (intInstance <= 1)
            {
                using (TrayIcon ti = new TrayIcon())
                {
                    ti.Display();
                    Application.Run();
                }
            }
            else
            {
                MessageBox.Show("Another instance of Scan-To-Print Monitor\r\nis already running.","Scan-To-Print-Monitor");
                Application.Exit();
            }
        }
    }
}