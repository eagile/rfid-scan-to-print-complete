﻿using AIOUSBNet;
using GiS_Commands;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Timers;
using System.Xml;


namespace Scan_To_Print
{
    public partial class Service1 : ServiceBase
    {
        //Acces I/O Variables
        private static UInt32 DeviceIndex = AIOUSB.diOnly;
        private static UInt32 LastIOError = 0;
        private static bool LastIOConnected = true;

        Timer aTimer;

        //GiS Variables
        private static string strGiSMode;
        private static string strGiSCOMName;
        private static int intHandle;
        private static sbyte[] sbtReaders = new sbyte[255];

        //---Settings.xml Variables---       
        private static int hornDuration;
        public static string suffixCharacters;
        public static bool comPortOpen;

        //Serial Port Variables
        private static string printerPort;
        private static int baudRate;
        private static Parity parityP;
        private static int dataBits;
        private static StopBits stopBitsS;

        private static uint lastGetDevicesResult = uint.MaxValue;

        //Application Log Location
        public static string path = AppDomain.CurrentDomain.BaseDirectory;
        const string STR_USER_PATH = @"C:\eAgile\Scan-To-Print\";
        const string STR_SERVICE = "Scan-To-Print";

        private static string[] suffixArray = new string[] { "CRLF", "TAB", "SPACE", "ESC" };

        //tag static password
        public static byte[] tagPassword = new byte[] { 0, 76, 70, 0 };

        //Boolean Condition Values for Reader Time Constraint
        private static string previousValue = "";        
        private static List<char> lstSuffixChars = new List<char>();        

        public Service1()
        {
            InitializeComponent();
        }

        #region ##### STARTUP #####

        protected override void OnStart(string[] args)        
        {
            aTimer = new Timer();
            string strVer;

            try
            {
                strVer = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
                WriteLog("App Verison:" + strVer, System.Reflection.MethodBase.GetCurrentMethod().Name);
                WriteLog("DLL Verison:" + GIS_LF_API.TSLF_LibVersion().ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name);
                WriteLog("Service Starting...", System.Reflection.MethodBase.GetCurrentMethod().Name);
                if (ConfigureFileStructure()) // Ensure proper folder/file structure exist.
                {
                    WriteLog("Configuration Success", System.Reflection.MethodBase.GetCurrentMethod().Name);
                    if (LoadSettingsFile()) // Load variables from settings.xml file.
                    {
                        WriteLog("Settings Success", System.Reflection.MethodBase.GetCurrentMethod().Name);
                        if (GetSuffixCharacters()) // Validate suffix characters.
                        {
                            WriteLog("Sufix Validated", System.Reflection.MethodBase.GetCurrentMethod().Name);
                            if (checkPortConnection()) // Validate availability of printer port.
                            {
                                WriteLog("Printer Port Success", System.Reflection.MethodBase.GetCurrentMethod().Name);
                                if (ResetIODevice()) // Validate availability of I/O device.
                                {
                                    WriteLog("I/O Connection Success", System.Reflection.MethodBase.GetCurrentMethod().Name);
                                    if (GiSTest()) // Validate availability of GiS device.
                                    {
                                        WriteLog("GIS Connection Success", System.Reflection.MethodBase.GetCurrentMethod().Name);
                                        using (EventLog log = new EventLog("Application"))
                                        {
                                            log.Clear();                                           
                                        }
                                        WriteLog("Win Log Cleared", System.Reflection.MethodBase.GetCurrentMethod().Name);
                                        LightStackFun(); // Visible method of connection status.
                                        WriteLog("Service Started", System.Reflection.MethodBase.GetCurrentMethod().Name);
                                        //Check I/O input status every 300ms
                                        aTimer.Elapsed += new ElapsedEventHandler(checkInputEvent);
                                        aTimer.Interval = 300;
                                        aTimer.Enabled = true;                                                                               
                                    }
                                    else // GiS connection failed.
                                    {
                                        WriteLog("Could not establish GiS mbH connection", System.Reflection.MethodBase.GetCurrentMethod().Name);
                                        Stop();
                                    }
                                }
                                else // Access IO connection failed.
                                {
                                    WriteLog("Could not establish AccessIO connection", System.Reflection.MethodBase.GetCurrentMethod().Name);
                                    Stop();
                                }
                            }
                            else // Printer port connection failed.
                            {
                                Stop();
                            }
                        }
                        else // Invalid suffix character.
                        {
                            Stop();
                        }
                    }
                    else // Error loading settings.xml.
                    {
                        Stop();
                    }
                }
                else // Error configuring files/folders.
                {                    
                    Stop();
                }
            }
            catch
            {
                WriteLog("Failed to Start", System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
        }

        private bool ConfigureFileStructure()
        {
            bool blnReturn;

            try
            {
                blnReturn = false;          
                Directory.CreateDirectory(STR_USER_PATH);

                // Check for guide pdf, copy if not found.
                if (!File.Exists(STR_USER_PATH + "\\Scan-To-Print Configuration Guide_1.2.0.pdf"))
                {                    
                    if (File.Exists(path + "\\Scan-To-Print Configuration Guide_1.2.0.pdf"))
                    {
                        File.Copy(path + "\\Scan-To-Print Configuration Guide_1.2.0.pdf", STR_USER_PATH + "\\Scan-To-Print Configuration Guide_1.2.0.pdf");
                    }
                }

                // Check for settings file, copy if not found.
                if (!File.Exists(STR_USER_PATH + "\\Scan-To-Print_settings.xml")) 
                {                    
                    if (File.Exists(path + "\\Scan-To-Print_settings.xml"))
                    {
                        File.Copy(path + "\\Scan-To-Print_settings.xml", STR_USER_PATH + "\\Scan-To-Print_settings.xml");
                        blnReturn = true;
                    } 
                    else
                    {
                        WriteLog("Unable to locate settings file", System.Reflection.MethodBase.GetCurrentMethod().Name);
                    }                   
                }
                else
                {
                    blnReturn = true;
                }
                return blnReturn;              
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        private bool LoadSettingsFile()
        {
            bool blnReturn;

            try
            {
                XmlDocument xdocSettings = new XmlDocument();
                xdocSettings.Load(STR_USER_PATH + "\\Scan-To-Print_settings.xml");
                XmlNodeList xnlNodes = xdocSettings.SelectNodes("/applicationSettings/applicationVariables");

                foreach (XmlNode xn in xnlNodes)
                {         
                    strGiSMode = xn["LFReaderPort"].InnerXml;
                    printerPort = xn["printerPort"].InnerXml;
                    hornDuration = Int32.Parse(xn["hornDuration"].InnerXml);
                    suffixCharacters = xn["suffixCharacters"].InnerXml;
                    baudRate = Int32.Parse(xn["baudRate"].InnerXml);
                    string parityTest = xn["parityTest"].InnerXml;
                    dataBits = Int32.Parse(xn["dataBits"].InnerXml);
                    string stopBits = xn["stopBits"].InnerXml;

                    parityP = (Parity)Enum.Parse(typeof(Parity), parityTest, true);
                    stopBitsS = (StopBits)Enum.Parse(typeof(StopBits), stopBits, true);
                }
                switch(strGiSMode.ToUpper())
                {
                    case "USB":
                        blnReturn =  true;
                        break;
                    default:
                        if(strGiSMode.ToUpper().Contains("COM"))
                        {
                            blnReturn = true;
                            strGiSCOMName = "\\\\.\\" + strGiSMode + " [GiS Virtual COM]";
                        }
                        else
                        {
                            blnReturn = false;
                            WriteLog("Invalid LFReaderPort value", System.Reflection.MethodInfo.GetCurrentMethod().Name);
                        }
                        break;
                }
                return blnReturn;
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString(), System.Reflection.MethodInfo.GetCurrentMethod().Name);
                return false;               
            }
        }

        private bool GetSuffixCharacters()
        {
            // Store required suffix characters

            bool blnReturn;

            switch(suffixCharacters)
            {
                case "CRLF":
                    lstSuffixChars.Add(Convert.ToChar(10));
                    lstSuffixChars.Add(Convert.ToChar(13));
                    blnReturn = true;
                    break;
                case "TAB":
                    lstSuffixChars.Add(Convert.ToChar(9));
                    blnReturn = true;
                    break;
                case "SPACE":
                    lstSuffixChars.Add(Convert.ToChar(32));
                    blnReturn = true;
                    break;
                case "ESC":
                    lstSuffixChars.Add(Convert.ToChar(27));
                    blnReturn = true;
                    break;
                default:                    
                    WriteLog("Incorrect suffix characters found in Settings.xml", System.Reflection.MethodBase.GetCurrentMethod().Name);
                    blnReturn = false;
                    break;
            }
            return blnReturn;         
        }
        #endregion

        #region ##### STOP SERVICE #####

        protected override void OnStop()
        {
            AIOUSB.ClearDevices();
            WriteLog("Service Stopped", System.Reflection.MethodBase.GetCurrentMethod().Name);            
            setAllOutputsLow();           
        }

        #endregion        

        #region ##### MAIN PROCESS ######

        //private enum IOState
        //{
        //    disconnected, // IO Device not found
        //    active, // Two inputs are pressed
        //    inactive, // Fewer than two inputs are pressed
        //}

        //private IOState GetIOState()
        //{
        //    UInt32 error = 0;
        //    IOState state = IOState.disconnected;
        //    try
        //    {
        //        byte[] bits = new byte[4];
        //        error = AIOUSB.DIO_ReadAll(DeviceIndex, bits);
        //        if (error > 0)
        //        {
        //            if (ResetIODevice())
        //            {
        //                error = AIOUSB.DIO_ReadAll(DeviceIndex, bits);
        //            }
        //        }
        //        if (error == 0)
        //        {
        //            int inputCount = 0;
        //            for (int i = 0; i < 8; i++)
        //            {
        //                if ((bits[2] & (1 << (i))) != 0)
        //                    inputCount++;
        //                if ((bits[3] & (1 << (i))) != 0)
        //                    inputCount++;
        //            }
        //            if (inputCount >= 2)
        //            {
        //                state = IOState.active;
        //            }
        //            else
        //            {
        //                state = IOState.inactive;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        WriteLog(ex.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name);
        //    }
        //    LastIOError = error;
        //    return state;
        //}

        private void checkInputEvent(object source, ElapsedEventArgs e)
        {
            aTimer.Stop();
            byte[] bits = new byte[4];
            UInt32 errorCode;

            try
            {
                // Read I/O current state
                errorCode = AIOUSB.DIO_ReadAll(DeviceIndex, bits); // First read attempt
                if (errorCode > 0)
                {
                    do
                    {
                        if (ResetIODevice() && AIOUSB.DIO_ReadAll(DeviceIndex, bits) == 0) // Reset and try again
                        {
                            errorCode = 0;
                        }
                        else if (ResetIODevice(1000)) // Wait 1 second and try again
                        {
                            errorCode = AIOUSB.DIO_ReadAll(DeviceIndex, bits);
                            if (errorCode > 0 && errorCode != LastIOError) // if still fails then log error code, diconnection will be logged
                            {
                                WriteLog("AIOUSB.DIO_ReadAll Return=" + errorCode.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name);
                            }
                            LastIOError = errorCode;
                        }
                        else // reset failed, device disconnected
                        {
                            errorCode = 1617; // ERROR_DEVICE_REMOVED
                        }

                        if (LastIOConnected && errorCode > 0)
                        {
                            LastIOConnected = false;
                            WriteLog("AccessIO Disconnected", System.Reflection.MethodBase.GetCurrentMethod().Name);
                            using (EventLog log = new EventLog("Application"))
                            {
                                log.Source = STR_SERVICE;
                                log.WriteEntry("AccessIO Disconnected ", EventLogEntryType.Information, 1);
                            }
                        }
                        else if (!LastIOConnected && errorCode == 0)
                        {
                            LastIOConnected = true;
                            WriteLog("AccessIO Reconnected", System.Reflection.MethodBase.GetCurrentMethod().Name);
                            using (EventLog log = new EventLog("Application"))
                            {
                                log.Source = STR_SERVICE;
                                log.WriteEntry("AccessIO Reconnected ", EventLogEntryType.Information, 1);
                            }
                            LightStackFun();
                            setAllOutputsLow();
                        }
                    } while (errorCode > 0); // Loop until we get a good response from I/O
                }
                LastIOConnected = true;

                //Test GiS connectivity
                if (GiSTest())
                {
                    string strCheck = ByteArrayToString(bits);
                    if (!strCheck.Substring(4, 4).Equals(previousValue))
                    {
                        //New I/O State found    
                        previousValue = strCheck.Substring(4, 4);
                        if (strCheck.Substring(5, 1).Equals("3"))
                        {
                            //Operator Presses 2 I/O Buttons = 3
                            setAllOutputsLow();
                            if (GisConnect())
                            {
                                if (GIS_LF_API.TSLF_Login(intHandle, 9, tagPassword, 4) == 0)
                                {
                                    string tagData = readTag(); // Application Reads RFID Tag Value.
                                    if (!tagData.Equals("Fail"))
                                    {
                                        GoodTag(tagData);
                                    }
                                    else
                                    {
                                        SignalFailure();
                                    }
                                }
                                else // Failed to login to tag with GiS.
                                {
                                    SignalFailure();
                                }
                                GisDisconnect();
                            }
                            else // Failed to connect to and reset GiS.
                            {
                                SignalFailure();
                            }
                            aTimer.Enabled = true; // Resume monitoring I/O.
                        }
                        else // I/O has new state but not "3" to start GiS, resume monitoring I/O
                        {
                            aTimer.Enabled = true;
                        }
                    }
                    else // I/O in same state, resume monitoring I/O
                    {
                        aTimer.Enabled = true;
                    }
                }
                else // Unable to locate GiS device, probably disconnected.
                {
                    WriteLog("GiS mbH Disconnected", System.Reflection.MethodBase.GetCurrentMethod().Name);
                    using (EventLog log = new EventLog("Application"))
                    {
                        log.Source = STR_SERVICE;
                        log.WriteEntry("GiS mbH Disconnected ", EventLogEntryType.Information, 1);
                    }
                    if (TryStartService("GiS") == "SUCCESS") // Block until GiS device returns.
                    {
                        using (EventLog log = new EventLog("Application"))
                        {
                            log.Source = STR_SERVICE;
                            log.WriteEntry("GiS mbH Reconnected ", EventLogEntryType.Information, 1);
                        }
                        LightStackFun();
                        setAllOutputsLow();
                        aTimer.Enabled = true;
                    }
                    else
                    {
                        // GiS recovery error, stop service.
                        Stop();
                    }
                }
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name);
                Stop();
            }
            finally
            {
                aTimer.Start();
            }
        }

        private string TryStartService(string device)
        {
            bool disconnected = true;
            byte[] bits = new byte[4];
            string strResult;

            try
            {
                strResult = "FAIL";
                if (device.Equals("GiS"))
                {
                    while (disconnected)
                    {
                        System.Threading.Thread.Sleep(1000);
                        if (GiSTest())
                        {
                            disconnected = false;
                            WriteLog("GiS mbH Reconnected", System.Reflection.MethodBase.GetCurrentMethod().Name);
                        }
                    }
                    strResult = "SUCCESS";
                }
                return strResult;
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name);
                return "ERROR";
            }
        }  
        
        private void GoodTag(string strData)
        {
            if (lstSuffixChars.Count() == 2)
            {
                if (WriteToPrinter(strData + lstSuffixChars[0] + lstSuffixChars[1]).Equals("Pass"))
                {
                    using (EventLog log = new EventLog("Application"))
                    {
                        log.Source = STR_SERVICE;
                        log.WriteEntry("Tag Data: " + strData, EventLogEntryType.Information, 1);
                    }
                    SignalSuccess();
                }
                else
                {
                    // Send to printer failed
                    using (EventLog log = new EventLog("Application"))
                    {
                        log.Source = STR_SERVICE;
                        log.WriteEntry("COM Port Disconnected", EventLogEntryType.Information, 1);                        
                    }
                    SignalFailure();
                }
            }
            else 
            {
                if (WriteToPrinter(strData + lstSuffixChars[0]).Equals("Pass"))
                {                    
                    using (EventLog log = new EventLog("Application"))
                    {
                        log.Source = STR_SERVICE;
                        log.WriteEntry("Tag Data: " + strData, EventLogEntryType.Information, 1);
                    }
                    SignalSuccess();
                }
                else
                {
                    // Send to printer failed
                    using (EventLog log = new EventLog("Application"))
                    {
                        log.Source = STR_SERVICE;
                        log.WriteEntry("COM Port Disconnected", EventLogEntryType.Information, 1);                        
                    }
                    SignalFailure();
                }
            }           
        }     
        #endregion

        #region ##### PRINTER PORT ######

        private bool checkPortConnection()
        {
            try
            {
                SerialPort serPort = new SerialPort(printerPort, baudRate, parityP, dataBits, stopBitsS);
                serPort.Open();
                System.Threading.Thread.Sleep(1000); // Wait before closing port after just opening port
                serPort.Close();
                return true;
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name);
                return false;
            }
        } 
       
        private string WriteToPrinter(string data)
        {
            //Write RFID serial value to COM port
            try
            {
                SerialPort port = new SerialPort(printerPort, baudRate, parityP, dataBits, stopBitsS);
                port.Open(); // Open the port for communications
                byte[] bytes = Encoding.ASCII.GetBytes(data); // Write bytes
                port.Write(bytes, 0, bytes.Length);
                System.Threading.Thread.Sleep(250); //possibly not required
                port.Close(); // Close the port                               
                return "Pass";
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name);
                return "Fail";
            }
        }

        #endregion

        #region ##### ACCES I/O ######

        private bool ResetIODevice(int msDelay = 0)
        {
            // Returns true if device was detected, else false
            if (msDelay > 0)
            {
                System.Threading.Thread.Sleep(msDelay);
            }
            try
            {
                AIOUSB.ClearDevices();
                uint devices = AIOUSB.GetDevices();
                if (devices != lastGetDevicesResult)
                {
                    WriteLog("AIOUSB.GetDevices=" + devices.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name);
                    lastGetDevicesResult = devices;
                }
                return (devices > 0);
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        private string ByteArrayToString(byte[] ba)
        {
            try
            {
                string hex = BitConverter.ToString(ba);
                return hex.Replace("-", "");
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name);
                return "ERROR";
            }
        }

        private void setAllOutputsLow()
        {
            try
            {
                UInt32 bits = 0;
                UInt32.TryParse("FFFFFFFF", NumberStyles.HexNumber, null as IFormatProvider, out bits);
                AIOUSB.DIO_WriteAll(DeviceIndex, ref bits);
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
        }

        private void SignalSuccess()
        {
            try
            {
                AIOUSB.DIO_Write1(DeviceIndex, 1, 0); // Output 1 high
            }
            catch(Exception ex)
            {
                WriteLog(ex.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
        }

        private void SignalFailure()
        {
            try
            {
                AIOUSB.DIO_Write1(DeviceIndex, 2, 0); //Output 2 high
                AIOUSB.DIO_Write1(DeviceIndex, 3, 0); //Output 3 high
                System.Threading.Thread.Sleep(hornDuration); //Sleep for x seconds
                AIOUSB.DIO_Write1(DeviceIndex, 3, 1); //Output 3 low                     
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
        }

        public void LightStackFun()
        {
            //Flash outputs 1 & 2 to indicate connection success
            try
            {
                setAllOutputsLow();
                for (int i = 0; i < 3; i++)
                {                    
                    AIOUSB.DIO_Write1(DeviceIndex, 2, 0); //Output 2 high
                    AIOUSB.DIO_Write1(DeviceIndex, 1, 0); //Output 1 High
                    System.Threading.Thread.Sleep(500); //Sleep for x seconds
                    AIOUSB.DIO_Write1(DeviceIndex, 2, 1); //Output 2 low
                    AIOUSB.DIO_Write1(DeviceIndex, 1, 1); //Output 3 low
                    System.Threading.Thread.Sleep(500); //Sleep for x seconds
                }
            }
            catch(Exception ex)
            {
                WriteLog(ex.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        #region ##### GiS #####

        private bool GiSTest()
        {
            string strTemp;
            int intResult;
            bool blnReturn;

            try
            {
                blnReturn = false;         
                switch(strGiSMode.ToUpper())
                {
                    case "USB":
                        intResult = GIS_LF_API.TSLF_GetUSBDeviceNames(sbtReaders, 255);                        
                        if(intResult > 1)
                        {                           
                            blnReturn = true;
                        }                        
                        break;
                    default:
                        intResult = GIS_LF_API.TSLF_GetCOMDeviceNames(sbtReaders, 255);                        
                        if (intResult > 0)
                        {
                            byte[] byteData = Array.ConvertAll(sbtReaders, (a) => (byte)a);
                            strTemp = Encoding.ASCII.GetString(byteData);                           
                            if (strTemp.Contains(strGiSCOMName))
                            {
                                blnReturn = true;
                            }                            
                        }                        
                        break;
                }
                return blnReturn;                                   
            }
            catch(Exception ex)
            {
                WriteLog(ex.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }       

        private bool GisConnect()
        {
            int intResponse, intI;
            bool blnReturn;
            string strTemp;
            sbyte[] sReader = new sbyte[255];

            try
            {
                blnReturn = false;
                switch (strGiSMode)
                {
                    case "USB":
                        sReader = sbtReaders;
                        break;
                    default:
                        intI = 0;
                        foreach (char c in strGiSCOMName)
                        {
                            strTemp = Convert.ToInt32(c).ToString();
                            sReader[intI] = Convert.ToSByte(strTemp);
                            intI++;
                        }
                        break;
                }
                
                intHandle = GIS_LF_API.TSLF_Open(sReader, 19200, 0, 1000);
                if (intHandle > 0)
                {
                    intResponse = GIS_LF_API.TSLF_SetIO(intHandle, 224, 128); // Yellow LED
                    if (intResponse == 0)
                    {
                        intResponse = GIS_LF_API.TSLF_ResetTransponder(intHandle, 9);
                        if (intResponse == 0)
                        {
                            blnReturn = true;
                        }
                        else
                        {
                            GIS_LF_API.TSLF_Close(intHandle);
                        }
                    }
                    else
                    {
                        GIS_LF_API.TSLF_Close(intHandle);
                    }
                }
                return blnReturn;
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        private void GisDisconnect()
        {
            try
            {
                GIS_LF_API.TSLF_Close(intHandle);
            }
            catch
            {

            }
        }

        private string readTag()
        {
            byte[] bytRead = new byte[4];
            int intResponse;
            string strWrite;
            List<string> tagList = new List<string>();
            List<string> checkSumList = new List<string>();

            try
            {
                for (int i = 0; i < 3; i++)
                {
                    intResponse = GIS_LF_API.TSLF_Read(intHandle, 9, 1, bytRead, 4);
                    if (intResponse > 0)
                    {
                        strWrite = FormatResult(bytRead);
                        if (!strWrite.Equals("Fail"))
                        {
                            tagList.Add(strWrite);
                            checkSumList.Add(CalculateChecksum(strWrite));
                        }
                    }
                    else
                    {
                        //No tag found, Break.
                        break;
                    } 
                }
                if (tagList.Count == 3)
                {
                    //check if any values differ from others
                    if (checkList(tagList) && checkList(checkSumList))
                    {                        
                        return tagList[0]; //good tags with same values & checksums
                    }
                    else
                    {
                        return "Fail";
                    }
                }
                else
                {
                    return "Fail";
                }
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name);
                return "Fail";
            }
        }

        private string FormatResult(byte[] arr)
        {
            try
            {
                string strResult;
                Array.Reverse(arr);
                strResult = BitConverter.ToString(arr);
                strResult = strResult.Replace("-", "");
                return strResult;
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name);
                return "Fail";
            }
        }

        private string CalculateChecksum(string dataToCalculate)
        {
            byte[] byteToCalculate = Encoding.ASCII.GetBytes(dataToCalculate);
            int checksum = 0;
            foreach (byte chData in byteToCalculate)
            {
                checksum += chData;
            }
            checksum &= 0xff;
            return checksum.ToString("X2");
        }
                
        private bool checkList(List<string> elements)
        {
            // Function used to check if all items in list are equal.
            if (elements.Any(o => o != elements[0]))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        #endregion

        #region ##### LOGGING #####

        private void WriteLog(string logMessage, string function)
        {
            try
            {
                using (StreamWriter w = File.AppendText(STR_USER_PATH + "\\log.txt"))
                {
                    if (function.Equals("OnStop") || function.Equals("OnStart"))
                    {
                        w.Write(DateTime.Now + "   Warn ");
                    }
                    else
                    {
                        w.Write(DateTime.Now + "  Error ");
                    }
                    w.Write("[" + function + "]\t" + logMessage);
                    w.WriteLine();
                }
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
        }
        #endregion
    }  
}

//NOTE: To get .NET4 install to work with VS2015, the following files where changed
//Copy file with replace dpca.dll from C:\Program Files (x86)\Microsoft Visual Studio 10.0\Common7\Tools\Deployment to
//C:\Program Files (x86)\Microsoft Visual Studio 12.0\Common7\IDE\CommonExtensions\Microsoft\VSI\bin
